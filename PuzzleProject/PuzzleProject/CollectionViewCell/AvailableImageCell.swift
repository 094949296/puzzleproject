//
//  AvailableImageCell.swift
//  a
//
//  Created by Hayk Movsesyan on 12/24/18.
//  Copyright © 2018 Hayk Movsesyan. All rights reserved.
//

import UIKit

class AvailableImageCell: UICollectionViewCell {
    
    @IBOutlet weak var chackButton: UIButton!
    @IBOutlet weak var puzzleImage: UIImageView!
    @IBOutlet var starsImageViewCollection: [UIImageView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenSize = UIScreen.main.bounds.width
        self.widthAnchor.constraint(equalToConstant: screenSize / 2).isActive = true
        self.heightAnchor.constraint(equalTo: widthAnchor).isActive = true
        self.puzzleImage.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.puzzleImage.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.puzzleImage.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        self.puzzleImage.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.layer.cornerRadius = self.frame.size.width / 10
    }
    
}
