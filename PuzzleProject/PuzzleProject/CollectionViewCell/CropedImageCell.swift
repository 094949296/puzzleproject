//
//  ComponentImageCell.swift
//  AddAccessToPhotoGalery
//
//  Created by Yurik Mnatsakanyan on 12/29/18.
//  Copyright © 2018 Yurik Mnatsakanyan. All rights reserved.
//

import UIKit

class ComponentImageCell: UICollectionViewCell {
    var componentImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addSubView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.addSubView()
    }
    
    func addSubView() {
        if self.contentView.subviews.count == 0 {
            let dropView = UIView(frame: self.bounds)
            self.contentView.addSubview(dropView)
            componentImageView = UIImageView(frame: dropView.frame)
            dropView.addSubview(componentImageView)
        }
    }
}
