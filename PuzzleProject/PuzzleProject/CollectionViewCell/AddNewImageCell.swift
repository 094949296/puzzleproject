//
//  AddNewImageCell.swift
//  a
//
//  Created by Hayk Movsesyan on 12/24/18.
//  Copyright © 2018 Hayk Movsesyan. All rights reserved.
//

import UIKit

class AddNewImageCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = self.frame.size.width / 10
    }
    
}
