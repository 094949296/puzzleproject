//
//  AlertTitleTableViewCell
//  PuzzleProject
//
//  Created by Hayk Movsesyan on 1/22/19.
//  Copyright © 2019 Yurik Mnatsakanyan. All rights reserved.
//

import UIKit

class AlertTitleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
