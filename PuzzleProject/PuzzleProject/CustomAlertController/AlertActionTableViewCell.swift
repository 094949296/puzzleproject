//
//  AlertItemTableViewCell.swift
//  PuzzleProject
//
//  Created by Hayk Movsesyan on 1/21/19.
//  Copyright © 2019 Yurik Mnatsakanyan. All rights reserved.
//

import UIKit

class AlertActionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var actionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
