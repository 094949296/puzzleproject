//
//  PuzzleAlertViewController.swift
//  PuzzleProject
//
//  Created by Hayk Movsesyan on 1/21/19.
//  Copyright © 2019 Yurik Mnatsakanyan. All rights reserved.
//

import UIKit

protocol PuzzleAlertViewControllerDelegate: class {
    func didSelectItem(in controller: PuzzleAlertViewController, at index: Int)
}

final class PuzzleAlertViewController: UIViewController {
    @IBOutlet weak var alertTableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var alertTitle: String?
    var actions = [String]()
    var countOfTitle: Int!
    weak var delegate: PuzzleAlertViewControllerDelegate?
    
    init(with title: String?, items: [String]) {
        self.alertTitle = title
        self.actions.append(contentsOf: items)
        super.init(nibName: "PuzzleAlertViewController", bundle: nil)
        setupPresentation()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    func setupTableView() {
        if alertTitle == nil {
            countOfTitle = 0
        } else {
            countOfTitle = 1
        }
        tableViewHeightConstraint.constant = CGFloat((actions.count + countOfTitle) * 60)
        alertTableView.register(UINib(nibName: "AlertActionTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "AlertActionTableViewCell")
        alertTableView.register(UINib(nibName: "AlertTitleTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "AlertTitleTableViewCell")
    }
    
    func setupPresentation() {
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalPresentationStyle = .overCurrentContext
        self.modalTransitionStyle = .crossDissolve
    }
}

extension PuzzleAlertViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actions.count + countOfTitle
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 && alertTitle != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AlertTitleTableViewCell", for: indexPath) as! AlertTitleTableViewCell
            cell.titleLabel.text = self.alertTitle!
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AlertActionTableViewCell", for: indexPath) as! AlertActionTableViewCell
            cell.actionLabel.text = self.actions[indexPath.row - countOfTitle]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = delegate {
            tableView.cellForRow(at: indexPath)?.isSelected = false
            delegate.didSelectItem(in: self, at: indexPath.row)
        }
    }
    
}
