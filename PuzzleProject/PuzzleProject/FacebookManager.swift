//
//  FacebookManager.swift
//  PuzzleProject
//
//  Created by Yurik Mnatsakanyan on 1/21/19.
//  Copyright © 2019 Yurik Mnatsakanyan. All rights reserved.
//

import Foundation
import FBSDKLoginKit

class FacebookManager {
    static let shared = FBSDKLoginManager()
    
    public class func getUserData(complation: @escaping () -> Void) {
        if FBSDKAccessToken.current() != nil {
            FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields": "email, first_name, last_name, picture.type(large)"])?.start(completionHandler: { (connection, result, error) in
                if error == nil {
                    let results = result as! [String: Any]
                    User.currentUser.setUser(results)
                    print("-----------------------")
                    print(result!)
                    print("-------------------------")
                    complation()
                }
            })
        }
    }
    
}
