//
//  StyledLabel.swift
//  a
//
//  Created by Hayk Movsesyan on 12/25/18.
//  Copyright © 2018 Hayk Movsesyan. All rights reserved.
//

import UIKit

class StyledLabel: UILabel {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = self.frame.size.height / 2
        self.clipsToBounds = true
    }
        
}
