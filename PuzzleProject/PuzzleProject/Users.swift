//
//  User.swift
//  PuzzleProject
//
//  Created by Yurik Mnatsakanyan on 1/21/19.
//  Copyright © 2019 Yurik Mnatsakanyan. All rights reserved.
//

import Foundation
import UIKit
import FBSDKCoreKit

class User {
    var firstName: String?
    var lastName: String?
    var profilePicture: UIImage?
    var imageURL: String?
    static let currentUser = User()
    
    func setUser(_ result: [String: Any]?) {
        if result != nil {
            if let firstName = result!["first_name"] as? String  {
                self.firstName = firstName
            }
            if let lastName = result!["last_name"] as? String  {
                self.lastName = lastName
            }
            if let pictureDict = result!["picture"] as? [String: Any] {
                if let pictureData = pictureDict["data"] as? [String: Any] {
                    if let url = pictureData["url"] as? String {
                        let profileImage: UIImage = UIImage(data: try! Data(contentsOf: URL(string: url)!))!
                        self.imageURL = url
                        self.profilePicture = profileImage
                    }
                }
            }
        }
    }
    
    func resetUser() {
        self.firstName = nil
        self.lastName = nil
        self.profilePicture = nil
    }
}
