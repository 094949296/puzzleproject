//
//  PersistentController.swift
//  PuzzleProject
//
//  Created by Hayk Movsesyan on 1/13/19.
//  Copyright © 2019 Yurik Mnatsakanyan. All rights reserved.
//

import Foundation
import CoreData

class PersistenceController {
    
    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    var newBackgroundContext: NSManagedObjectContext {
        return persistentContainer.newBackgroundContext()
    }
    
    func saveMainContext() {
        let context = viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func saveContext(_ context: NSManagedObjectContext) {
        try? context.save()
        DispatchQueue.main.async {
            self.viewContext.refreshAllObjects()
        }
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "PuzzleProject")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
}
