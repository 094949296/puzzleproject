//
//  DataManager.swift
//  PuzzleProject
//
//  Created by Hayk Movsesyan on 1/13/19.
//  Copyright © 2019 Yurik Mnatsakanyan. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class DataManager {
    
    private let persistenceController: PersistenceController
    
    init(persistenceController: PersistenceController) {
        self.persistenceController = persistenceController
    }
    
    // Getting Puzzles
    func gettingPuzzles() -> [Puzzle] {
        let request: NSFetchRequest<Puzzle> = Puzzle.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        let puzzles = try? persistenceController.viewContext.fetch(request)
        return puzzles!
    }
    
    // Getting All Components
    func gettingAllComponents(for selectedPuzzle: Puzzle) -> [Component] {
        let puzzles = gettingPuzzles()
        var components: [Component]!
        for puzzle in puzzles {
            if puzzle == selectedPuzzle {
                components = puzzle.allComponents?.allObjects as? [Component]
            }
        }
        components.sort { (component1, component2) -> Bool in
            component1.position < component2.position
        }
        return components
    }
    
    // Getting Components With Position
    func gettingComponentsWithPosition(for selectedPuzzle: Puzzle) -> [ComponentWithPosition] {
        let puzzles = gettingPuzzles()
        var components: [ComponentWithPosition]!
        for puzzle in puzzles {
            if puzzle == selectedPuzzle {
                components = puzzle.componentsWithPosition?.allObjects as? [ComponentWithPosition]
            }
        }
        return components
    }
    
    // Getting Components Without Position
    func gettingComponentsWithPosition(for selectedPuzzle: Puzzle) -> [ComponentWithoutPosition] {
        let puzzles = gettingPuzzles()
        var components: [ComponentWithoutPosition]!
        for puzzle in puzzles {
            if puzzle == selectedPuzzle {
                components = puzzle.componentsWithoutPosition?.allObjects as? [ComponentWithoutPosition]
            }
        }
        return components
    }
    
    //Save Puzzle
    func savePuzzle(image: UIImage, complation: @escaping (Puzzle) -> Void) {
        let context = persistenceController.viewContext
        let puzzle = NSEntityDescription.insertNewObject(forEntityName: "Puzzle", into: context) as! Puzzle
        puzzle.image = image.pngData()
        puzzle.stars = 0
        puzzle.date = Date()
        puzzle.completed = true
        puzzle.checked = false
        complation(puzzle)
        persistenceController.saveMainContext()
    }
    
    //Save Components Without Position
    func saveComponentsWithoutPosition(for puzzleDate: Date, componentsImages: [UIImage]) {
        let context = persistenceController.viewContext
        let puzzle = fetchSelectedPuzzle(with: puzzleDate)
        if !componentsImages.isEmpty {
            puzzle.completed = false
        }
        for imageIndex in 0..<componentsImages.count {
            let component = NSEntityDescription.insertNewObject(forEntityName: "ComponentWithoutPosition", into: context) as! ComponentWithoutPosition
            component.image = componentsImages[imageIndex].pngData()
            component.id = componentsImages[imageIndex].accessibilityIdentifier
            puzzle.addToComponentsWithoutPosition(component)
        }
        persistenceController.saveMainContext()
    }
    
    //Save Component With Position
    func saveComponentWithPosition(for puzzleDate: Date, xCenter: CGFloat, yCenter: CGFloat, componentImage: UIImage) {
        let context = persistenceController.viewContext
        let puzzle = fetchSelectedPuzzle(with: puzzleDate)
        let component = NSEntityDescription.insertNewObject(forEntityName: "ComponentWithPosition", into: context) as! ComponentWithPosition
        component.xPosition = Float(xCenter)
        component.yPosition = Float(yCenter)
        component.image = componentImage.pngData()
        component.id = componentImage.accessibilityIdentifier
        puzzle.addToComponentsWithPosition(component)
        persistenceController.saveMainContext()
    }
    
    //Save All Components
    func saveAllComponents(for puzzleDate: Date, componentsImages: [UIImage]) {
        let context = persistenceController.viewContext
        let puzzle = fetchSelectedPuzzle(with: puzzleDate)
        if !componentsImages.isEmpty {
            puzzle.completed = false
        }
        for imageIndex in 0..<componentsImages.count {
            let component = NSEntityDescription.insertNewObject(forEntityName: "Component", into: context) as! Component
            componentsImages[imageIndex].accessibilityIdentifier = String(imageIndex)
            component.image = componentsImages[imageIndex].pngData()
            component.id = componentsImages[imageIndex].accessibilityIdentifier
            component.position = Int16(imageIndex)
            puzzle.addToAllComponents(component)
        }
        persistenceController.saveMainContext()
    }
    
    //Deleting Puzzles
    func deletePuzzle(puzzles: [Puzzle]) {
        let context = persistenceController.viewContext
        for puzzle in puzzles {
            context.delete(puzzle)
        }
        persistenceController.saveMainContext()
    }
    
    //fetch Selected Puzzle
    private func fetchSelectedPuzzle(with date: Date) -> Puzzle {
        let puzzles = fetchPuzzles()
        var selectedPuzzle = Puzzle()
        for puzzle in puzzles! {
            if puzzle.value(forKey: "date") as! Date == date {
                selectedPuzzle = puzzle
                break
            }
        }
        return selectedPuzzle
    }
    
    //Fetch Puzzles
    private func fetchPuzzles() -> [Puzzle]? {
        let request: NSFetchRequest<Puzzle> = Puzzle.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        let object = try? persistenceController.viewContext.fetch(request)
        return object
    }
    
    //Change Puzzle As Complated
    func savePuzzleAsComplated(changedPuzzle: Puzzle, stars: Int16) {
        let context = persistenceController.viewContext
        let allPuzzles = fetchPuzzles()
        for puzzle in allPuzzles! {
            if puzzle.date == changedPuzzle.date {
                puzzle.allComponents = NSSet()
                puzzle.componentsWithPosition = NSSet()
                puzzle.componentsWithoutPosition = NSSet()
                puzzle.completed = true
                if stars > changedPuzzle.stars {
                    puzzle.stars = stars
                }
                try? context.save()
            }
        }
        persistenceController.saveMainContext()
    }
    
    //Save Number Of Components in line
    func save(for changedPuzzle: Puzzle, countOfPuzzlesInEveryLines: Int, time: Int16) {
        let context = persistenceController.viewContext
        let allPuzzles = fetchPuzzles()
        for puzzle in allPuzzles! {
            if puzzle.date == changedPuzzle.date {
                puzzle.numberOfPuzzlesInEveryLines = Int16(countOfPuzzlesInEveryLines)
                puzzle.complationTime = time
                try? context.save()
            }
        }
        persistenceController.saveMainContext()
    }
    
    //Reset Puzzle Settings
    func resetPuzzleSettings(for changedPuzzle: Puzzle) {
        let context = persistenceController.viewContext
        let allPuzzles = fetchPuzzles()
        for puzzle in allPuzzles! {
            if puzzle.date == changedPuzzle.date {
                puzzle.allComponents = NSSet()
                puzzle.componentsWithPosition = NSSet()
                puzzle.componentsWithoutPosition = NSSet()
                puzzle.completed = true
                try? context.save()
            }
        }
        persistenceController.saveMainContext()
    }
}
