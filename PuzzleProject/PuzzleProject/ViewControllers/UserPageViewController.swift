//
//  UserPageViewController.swift
//  PuzzleProject
//
//  Created by Yurik Mnatsakanyan on 1/18/19.
//  Copyright © 2019 Yurik Mnatsakanyan. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class UserPageViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var familyNameLabel: UILabel!
    var profilePictureURL: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImageView.layer.cornerRadius = CGFloat(profileImageView.frame.size.width / 2)
        profileImageView.clipsToBounds = true
        profileImageView.layer.borderColor = UIColor.lightGray.cgColor
        profileImageView.layer.borderWidth = 4
        if User.currentUser.imageURL != nil {
            profilePictureURL = User.currentUser.imageURL
        }
        profileImageView.image = User.currentUser.profilePicture
        nameLabel.text = User.currentUser.firstName
        familyNameLabel.text = User.currentUser.lastName
    }
    
    @IBAction func makeProfileImageAsPuzzle() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SelectedImageViewController") as! SelectedImageViewController
        if profileImageView.image!.size.width > profileImageView.image!.size.height {
            vc.selectedImage = profileImageView.image!.centerCrop()
            UIApplication.dataManager.savePuzzle(image: profileImageView.image!) { (addedPuzzle) in
                vc.puzzle = addedPuzzle
            }
        } else {
            vc.selectedImage = profileImageView.image
            UIApplication.dataManager.savePuzzle(image: profileImageView.image!) { (addedPuzzle) in
                vc.puzzle = addedPuzzle
            }
        }
        navigationController?.setViewControllers([vc, self], animated: false)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func alreadyCreatedPuzzles() {
        
    }
    
    @IBAction func logOut() {
        FacebookManager.shared.logOut()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func playGame() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SuggestedImagesViewController") as! SuggestedImagesViewController
        navigationController?.pushViewController(vc, animated: true)
    }
}
