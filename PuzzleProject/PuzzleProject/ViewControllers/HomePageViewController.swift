//
//  HomePageViewController.swift
//  Created by Hayk Movsesyan on 12/4/18.
//  Copyright © 2018 Hayk Movsesyan. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import Foundation

class HomePageViewController: UIViewController {
    
    @IBOutlet var animatedPuzzlesCollection: [UIImageView]!
    @IBOutlet weak var leftAnimatedBackground: UIImageView!
    @IBOutlet weak var rightAnimatedBackground: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    var animatedPuzzleHeight: CGFloat!
    var animatedPuzzleWidth: CGFloat!
    var centerDistantionBetweenPuzzles: CGFloat!
    var firstLineAnimatedPuzzlesYCoordinate: CGFloat!
    var secondLineAnimatedPuzzlesYCoordinate: CGFloat!
    var firstAnimatedPuzzleCenterXFromLeft: CGFloat!
    var puzzleHoleSize: CGFloat!
    var gatheredPuzzleWidth: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutAnimatedPuzzles()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateHomePage()
    }
    
    @IBAction func playTupped() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SuggestedImagesViewController") as! SuggestedImagesViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func fbTupped() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserPageViewController") as! UserPageViewController
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile"], from: self) { (result, error) in
            if error == nil {
                FacebookManager.getUserData(complation: {
                    self.dismiss(animated: false, completion: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                })
            }
        }
    }
    
    private func layoutAnimatedPuzzles() {
        animatedPuzzleWidth = animatedPuzzlesCollection[0].frame.width
        animatedPuzzleHeight = animatedPuzzlesCollection[0].frame.height
        centerDistantionBetweenPuzzles = (animatedPuzzleHeight + animatedPuzzleWidth) / 2 - animatedPuzzleHeight * 207 / 497
        firstLineAnimatedPuzzlesYCoordinate = view.frame.height / 2 - (animatedPuzzleHeight / 2 - animatedPuzzleHeight * 42 / 497)
        secondLineAnimatedPuzzlesYCoordinate = view.frame.height / 2 + (animatedPuzzleHeight / 2 - animatedPuzzleHeight * 42 / 497)
        puzzleHoleSize = animatedPuzzleHeight * 190 / 497
        gatheredPuzzleWidth = (3 * animatedPuzzleWidth) + (2 * animatedPuzzleHeight)
        firstAnimatedPuzzleCenterXFromLeft = (view.frame.width - gatheredPuzzleWidth + (8 * puzzleHoleSize)) / 2
    }
    
    private func animateHomePage() {
        UIView.animate(withDuration: 1, animations: {
            self.animatePuzzle()
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                UIView.animate(withDuration: 1, animations: {
                    self.animateBackground()
                }) { (finished) in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                        self.animateButtons()
                    })
                }
            })
        }
    }
    
    private func animatePuzzle() {
        for puzzleIndex in 0 ..< animatedPuzzlesCollection.count {if puzzleIndex < 5 {
            animatedPuzzlesCollection[puzzleIndex].center = CGPoint(x: firstAnimatedPuzzleCenterXFromLeft + CGFloat(puzzleIndex) * centerDistantionBetweenPuzzles, y: firstLineAnimatedPuzzlesYCoordinate)
        } else {
            animatedPuzzlesCollection[puzzleIndex].center = CGPoint(x: firstAnimatedPuzzleCenterXFromLeft + CGFloat(puzzleIndex - 5) * centerDistantionBetweenPuzzles, y: secondLineAnimatedPuzzlesYCoordinate)
            }
        }
    }
    
    private func animateBackground() {
        leftAnimatedBackground.center = CGPoint(x: view.frame.width / 2, y: view.frame.height / 2)
        rightAnimatedBackground.center = CGPoint(x: view.frame.width / 2, y: view.frame.height / 2)
    }
    
    private func animateButtons() {
        UIView.animate(withDuration: 1, animations: {
            self.playButton.center = CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2)
            self.facebookButton.center = CGPoint(x: self.view.frame.width/2, y: 7 / 9 * self.view.frame.height)
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                self.facebookButton.animate()
                self.playButton.animate()
            })
        }
    }
}


