//
//  SelectedImageViewController.swift
//  Created by Yurik Mnatsakanyan on 12/29/18.
//  Copyright © 2018 Yurik Mnatsakanyan. All rights reserved.
//

import UIKit
import Darwin

class SelectedImageViewController: UIViewController {
    
    @IBOutlet weak var puzzleImageView: UIImageView!
    @IBOutlet weak var numberOfCropedImagesLabel: StyledLabel!
    var selectedImage: UIImage!
    var puzzle: Puzzle!
    var componentsCounts = [9, 16, 25, 36, 49]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        puzzleImageView.image = selectedImage
    }
    
    @IBAction func changeNumberOfCropedImages(_ sender: UIButton) {
        if sender.titleLabel?.text == "-" {
            if numberOfCropedImagesLabel.text != "9" {
                for i in 0..<componentsCounts.count {
                    if Int(numberOfCropedImagesLabel.text!) == componentsCounts[i] {
                        numberOfCropedImagesLabel.text = String(componentsCounts[i - 1])
                        break
                    }
                }
            }
        } else {
            if numberOfCropedImagesLabel.text != "49" {
                for i in 0..<componentsCounts.count {
                    if Int(numberOfCropedImagesLabel.text!) == componentsCounts[i] {
                        numberOfCropedImagesLabel.text = String(componentsCounts[i + 1])
                        break
                    }
                }
            }
        }
    }
    
    @IBAction func backToPreviousPage() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func goToCropeImage() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CropedImageViewController") as! CropedImageViewController
        vc.puzzle = self.puzzle
        vc.componentsCount = Int(self.numberOfCropedImagesLabel.text!)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


