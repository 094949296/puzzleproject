//
//  CropedImageViewController.swift
//  Created by Yurik Mnatsakanyan on 12/29/18.
//  Copyright © 2018 Yurik Mnatsakanyan. All rights reserved.
//

import UIKit
import GoogleMobileAds

class CropedImageViewController: UIViewController {
    
    @IBOutlet weak var PuzzleImageViewWidthAnchor: NSLayoutConstraint!
    @IBOutlet weak var puzzleImageView: UIImageView!
    @IBOutlet weak var dropView: UIView!
    @IBOutlet weak var componentsCollectionView: UICollectionView!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var timerLabel: UILabel!
    var puzzle: Puzzle!
    var startedNewGame = true
    var componentsCount: Int!
    private var dragView: UIView!
    private var timer: Timer!
    private var componentsImageSet: Set<UIImage> = []
    private var componentsImageArray: [UIImage] = []
    private var componentsImagesSortedArray: [UIImage] = []
    private var indexPathOfDragedCell: IndexPath!
    private var countOfPuzzlesInEveryLine: Int!
    private var componentsYCentersInDropView = [CGFloat]()
    private var componentsXCentersInDropView = [CGFloat]()
    private var dragViewIsOutOfCollectionViewPlace = false
    private var puzzleComplated = false
    private var restartsGame = false
    private var seconds: Int16!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        if startedNewGame == true {
            startNewGame()
        } else {
            cuntinueGame()
        }
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerDown), userInfo: nil, repeats: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        calculatingComponentCentersOnDropView()
        NotificationCenter.default.addObserver(self, selector: #selector(saveData), name: UIApplication.willTerminateNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(stopTimer), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(resumeTimer), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @IBAction func backToPreviousPage() {
        UIApplication.dataManager.savePuzzleAsComplated(changedPuzzle: puzzle, stars: puzzle!.stars)
        navigationController?.popViewController(animated: true)
    }
    
    func addLongPressGesture(view: UIView) {
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.didLongPressCell (recognizer:)))
        longPress.minimumPressDuration = 0.1
        view.addGestureRecognizer(longPress)
    }
    
    @objc func didLongPressCell (recognizer: UILongPressGestureRecognizer) {
        switch recognizer.state {
        case .began:
            createViewForDragging(recognizer: recognizer)
            view.isUserInteractionEnabled = false
        case .changed:
            changeDragViewState(recognizer: recognizer)
        case .ended:
            if (dragView == nil) {return}
            if (dragView.center.y > puzzleImageView.frame.minY && dragView.center.y < puzzleImageView.frame.maxY) {
                dragViewDropOnDropView()
            } else {
                dragViewReturnToCollectionView(recognizer: recognizer)
            }
            dragViewIsOutOfCollectionViewPlace = false
            view.isUserInteractionEnabled = true
        default:
            break
        }
    }
    
    @objc func saveData(notification: NSNotification) {
        if puzzleComplated != true {
            UIApplication.dataManager.resetPuzzleSettings(for: puzzle)
            UIApplication.dataManager.saveComponentsWithoutPosition(for: puzzle.date!, componentsImages: componentsImageArray)
            UIApplication.dataManager.save(for: puzzle, countOfPuzzlesInEveryLines: countOfPuzzlesInEveryLine, time: seconds)
            for view in dropView.subviews {
                UIApplication.dataManager.saveComponentWithPosition(for: puzzle.date!, xCenter: view.center.x, yCenter: view.center.y, componentImage: (view.subviews[0] as! UIImageView).image!)
            }
        } else {
            UIApplication.dataManager.resetPuzzleSettings(for: puzzle)
        }
    }
    
    @objc func timerDown() {
        if puzzleComplated == false {
            seconds -= 1
            if seconds >= 0 {
                timerLabel.text = timeString(time: TimeInterval(seconds))
                if seconds <= 10 {
                    timerLabel.backgroundColor = UIColor.red
                    let animation = CASpringAnimation(keyPath: "transform.scale")
                    animation.duration = 1
                    animation.fromValue = 0.9
                    animation.toValue = 1
                    timerLabel.layer.add(animation, forKey: nil)
                }
            } else if seconds == -1 {
                let alert = PuzzleAlertViewController.init(with: "You Lose", items: ["Start New Game", "Restart"])
                alert.delegate = self
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @objc func stopTimer() {
        timer.invalidate()
    }
    
    @objc func resumeTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerDown), userInfo: nil, repeats: true)
    }
    
    private func createViewForDragging(recognizer: UIGestureRecognizer) {
        if let cellView = recognizer.view {
            let frame = cellView.convert(cellView.frame, to: view)
            dragView = cellView
            dragView.frame = frame
            dragView.center = recognizer.location(in: view)
            view.addSubview(dragView!)
            if componentsCollectionView.frame.intersects(dragView.frame) {
                indexPathOfDragedCell = componentsCollectionView.indexPathForItem(at: recognizer.location(in: componentsCollectionView))
                if indexPathOfDragedCell != nil {
                    componentsImageArray.remove(at: indexPathOfDragedCell.row)
                }
            }
        }
    }
    
    private func changeDragViewState(recognizer: UIGestureRecognizer) {
        if dragView != nil {
            dragView?.center = recognizer.location(in: view)
            if !componentsCollectionView.frame.intersects(CGRect(x: dragView.center.x, y: dragView.center.y, width: 1, height: 1)) && dragViewIsOutOfCollectionViewPlace == false {
                dragView.frame.size.height = puzzleImageView.frame.height * CGFloat(7) / CGFloat(countOfPuzzlesInEveryLine * 5)
                dragView.frame.size.width = puzzleImageView.frame.width * CGFloat(7) / CGFloat(countOfPuzzlesInEveryLine * 5)
                dragView.subviews[0].frame.size.height = puzzleImageView.frame.height * CGFloat(7) / CGFloat(countOfPuzzlesInEveryLine * 5)
                dragView.subviews[0].frame.size.width = puzzleImageView.frame.width * CGFloat(7) / CGFloat(countOfPuzzlesInEveryLine * 5)
                if indexPathOfDragedCell != nil {
                    (dragView.subviews[0] as! UIImageView).cropAsPuzzle(count: countOfPuzzlesInEveryLine)
                    componentsCollectionView.deleteItems(at: [indexPathOfDragedCell])
                }
                indexPathOfDragedCell = nil
                dragViewIsOutOfCollectionViewPlace = true
            }
        }
    }
    
    private func dragViewDropOnDropView() {
        getDragViewCenterForDroppingInDropView()
        dropView.addSubview(dragView)
        setTagToDragViewAfterDroppingImDropView()
        droppingWithAnimattionOnDropView()
    }
    
    private func getDragViewCenterForDroppingInDropView() {
        for i in 0..<countOfPuzzlesInEveryLine {
            if dragView.center.x - componentsXCentersInDropView[i] < dragView.frame.width / 2  || componentsXCentersInDropView[i] - dragView.center.x > -dragView.frame.width / 2 {
                dragView.center.x = componentsXCentersInDropView[i]
                break
            }
        }
        for i in 0..<countOfPuzzlesInEveryLine {
            if dragView.center.y - componentsYCentersInDropView[i] < dragView.frame.width / 2 || componentsYCentersInDropView[i] - dragView.center.y > -dragView.frame.width / 2 {
                dragView.center.y = componentsYCentersInDropView[i] - puzzleImageView.frame.minY
                break
            }
        }
    }
    
    private func setTagToDragViewAfterDroppingImDropView() {
        let xPosition = Int(dragView.center.x * CGFloat(countOfPuzzlesInEveryLine) / dropView.frame.height)
        let yPosition = Int(dragView.center.y * CGFloat(countOfPuzzlesInEveryLine) / dropView.frame.width)
        dragView.tag = xPosition + yPosition * countOfPuzzlesInEveryLine
    }
    
    private func droppingWithAnimattionOnDropView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.dragView.alpha = 0.2
            self.dragView.alpha = 1
        }) { (finished) in
            if self.componentsImageArray.isEmpty {
                var puzzleIsComplated = true
                for view in self.dropView.subviews {
                    if view.tag != (view.subviews[0] as! UIImageView).tag {
                        puzzleIsComplated = false
                        break
                    }
                }
                if puzzleIsComplated == true {
                    self.puzzleComplated = true
                    self.componentsCollectionView.isHidden = true
                    self.showWinAlert()
                    UIApplication.dataManager.savePuzzleAsComplated(changedPuzzle: self.puzzle, stars: Int16(self.countOfPuzzlesInEveryLine - 2))
                }
            }
        }
    }
    
    private func showWinAlert() {
        let winAlert = self.storyboard?.instantiateViewController(withIdentifier: "ShowCupAlertController") as! ShowCupAlertController
        winAlert.newStars = self.countOfPuzzlesInEveryLine - 2
        winAlert.oldStars = Int(self.puzzle.stars)
        winAlert.providesPresentationContextTransitionStyle = true
        winAlert.definesPresentationContext = true
        winAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        winAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        winAlert.delegate = self
        self.present(winAlert, animated: true, completion: nil)
    }
    
    private func dragViewReturnToCollectionView(recognizer: UIGestureRecognizer) {
        let locationForDropping = gettingCoordinatesForDroppingOnCollectionView(for: recognizer).0
        let indexPathForDropping = gettingCoordinatesForDroppingOnCollectionView(for: recognizer).1
        droppingwithAnimation(in: locationForDropping, for: indexPathForDropping)
    }
    
    private func gettingCoordinatesForDroppingOnCollectionView(for recognizer: UIGestureRecognizer) -> (CGPoint, IndexPath) {
        var dropToIndexPath: IndexPath!
        var goToLocation: CGPoint!
        let collectionViewCenter = view.convert(componentsCollectionView.center, to: componentsCollectionView)
        if let indexPath = componentsCollectionView.indexPathForItem(at: CGPoint(x: recognizer.location(in: componentsCollectionView).x, y: collectionViewCenter.y)) {
            dropToIndexPath = indexPath
        } else if let indexPath = componentsCollectionView.indexPathForItem(at: CGPoint(x: recognizer.location(in: componentsCollectionView).x - CGFloat(6), y: collectionViewCenter.y)) {
            dropToIndexPath = indexPath
        } else if let indexPath = componentsCollectionView.indexPathForItem(at: CGPoint(x: recognizer.location(in: componentsCollectionView).x + CGFloat(6), y: collectionViewCenter.y)) {
            dropToIndexPath = indexPath
        }
        if dropToIndexPath != nil {
            goToLocation = componentsCollectionView.convert(componentsCollectionView.cellForItem(at: dropToIndexPath)!.center, to: view)
        } else {
            goToLocation = CGPoint(x: 40, y: collectionViewCenter.y)
            dropToIndexPath = IndexPath(row: 0, section: 0)
        }
        return (goToLocation, dropToIndexPath)
    }
    
    private func droppingwithAnimation(in location: CGPoint, for indexPath: IndexPath) {
        UIView.animate(withDuration: TimeInterval(0.2), animations: {
            self.dragView.center.x = location.x
            self.dragView.center.y = self.componentsCollectionView.center.y
            let imageForcreatingCell = UIImage()
            imageForcreatingCell.accessibilityIdentifier = (self.dragView.subviews[0] as! UIImageView).image!.accessibilityIdentifier
            if self.dragViewIsOutOfCollectionViewPlace == false {
                self.componentsCollectionView.deleteItems(at: [self.indexPathOfDragedCell])
            }
            self.componentsImageArray.insert(imageForcreatingCell, at: indexPath.row)
            self.componentsCollectionView.insertItems(at: [indexPath])
            self.componentsImageArray[indexPath.row] = (self.dragView.subviews[0] as! UIImageView).image!
        }) { (finished) in
            UIView.animate(withDuration: 0.1, animations: {
                self.dragView.transform = CGAffineTransform(scaleX: 80 / self.dragView.frame.width, y: 80 / self.dragView.frame.height)
            }) { (finished) in
                (self.componentsCollectionView.cellForItem(at: indexPath) as! ComponentImageCell).componentImageView.image = (self.dragView.subviews[0] as! UIImageView).image
                self.dragView.removeFromSuperview()
            }
        }
    }
    
    private func startNewGame() {
        countOfPuzzlesInEveryLine = Int(sqrt(Double(componentsCount)))
        puzzle.complationTime = Int16(componentsCount * 4)
        timerLabel.text = timeString(time: TimeInterval(puzzle.complationTime))
        seconds = puzzle.complationTime
        if Int(view.frame.width) % countOfPuzzlesInEveryLine != 0 {
            PuzzleImageViewWidthAnchor.constant = CGFloat(Int(view.frame.width) - Int(view.frame.width) % countOfPuzzlesInEveryLine + countOfPuzzlesInEveryLine)
        } else {
            PuzzleImageViewWidthAnchor.constant = view.frame.width
        }
        puzzleImageView.image = UIImage(data: puzzle.image!)
        if restartsGame == false {
            cropeGivenImage()
        } else {
            componentsImageArray = Array(componentsImageSet)
            componentsCollectionView.reloadData()
        }
    }
    
    private func cuntinueGame() {
        countOfPuzzlesInEveryLine = Int(puzzle.numberOfPuzzlesInEveryLines)
        calculatePuzzleImageViewSizes()
        setComponentsWithPosition()
        setComponentsWithoutPosition()
        seconds = puzzle.complationTime
    }
    
    private func calculatePuzzleImageViewSizes() {
        let puzzleImage = UIImage(data: puzzle.image!)
        if Int(view.frame.width) % countOfPuzzlesInEveryLine != 0 {
            PuzzleImageViewWidthAnchor.constant = CGFloat(Int(view.frame.width) - Int(view.frame.width) % countOfPuzzlesInEveryLine + countOfPuzzlesInEveryLine)
        } else {
            PuzzleImageViewWidthAnchor.constant = view.frame.width
        }
        let widthInPixels = puzzleImage!.size.width * puzzleImage!.scale
        let heightInPixels = puzzleImage!.size.height * puzzleImage!.scale
        puzzleImageView.image = puzzleImage!.cropImage(x: widthInPixels / CGFloat(countOfPuzzlesInEveryLine * 5 + 2),
                                                       y: heightInPixels / CGFloat(countOfPuzzlesInEveryLine * 5 + 2),
                                                       width: widthInPixels * CGFloat(countOfPuzzlesInEveryLine * 5) / CGFloat(countOfPuzzlesInEveryLine * 5 + 2),
                                                       height: heightInPixels * CGFloat(countOfPuzzlesInEveryLine * 5) / CGFloat(countOfPuzzlesInEveryLine * 5 + 2))
    }
    
    private func setComponentsWithPosition() {
        for componentWithPosition in (puzzle.componentsWithPosition?.allObjects as! [ComponentWithPosition]) {
            let view = UIView(frame: .zero)
            view.frame.size.height = puzzleImageView.frame.height * CGFloat(7) / CGFloat(countOfPuzzlesInEveryLine * 5)
            view.frame.size.width = puzzleImageView.frame.width * CGFloat(7) / CGFloat(countOfPuzzlesInEveryLine * 5)
            view.frame.size.height = dropView.frame.width * CGFloat(1.2) / CGFloat(countOfPuzzlesInEveryLine)
            view.frame.size.width = dropView.frame.width * CGFloat(1.2) / CGFloat(countOfPuzzlesInEveryLine)
            view.center.x = CGFloat(componentWithPosition.xPosition)
            view.center.y = CGFloat(componentWithPosition.yPosition)
            addLongPressGesture(view: view)
            view.tag = Int(componentWithPosition.id!)!
            let imageView = UIImageView(frame: view.bounds)
            imageView.image = UIImage(data: componentWithPosition.image!)
            imageView.image!.accessibilityIdentifier = componentWithPosition.id
            imageView.tag = Int(componentWithPosition.id!)!
            view.addSubview(imageView)
            dropView.addSubview(view)
            imageView.cropAsPuzzle(count: countOfPuzzlesInEveryLine)
        }
    }
    
    private func setComponentsWithoutPosition() {
        for componentWithoutPosition in (puzzle.componentsWithoutPosition?.allObjects as! [ComponentWithoutPosition]) {
            let componentImage = UIImage(data: componentWithoutPosition.image!)
            componentImage!.accessibilityIdentifier = componentWithoutPosition.id
            componentsImageArray.append(componentImage!)
        }
    }
    
    private func calculatingComponentCentersOnDropView() {
        for i in 0..<countOfPuzzlesInEveryLine {
            let imageWidth = puzzleImageView.frame.width / CGFloat(countOfPuzzlesInEveryLine)
            componentsXCentersInDropView.append(imageWidth / 2 + CGFloat(i) * imageWidth)
        }
        for i in 0..<countOfPuzzlesInEveryLine {
            let imageHeight = puzzleImageView.frame.height / CGFloat(countOfPuzzlesInEveryLine)
            componentsYCentersInDropView.append(puzzleImageView.frame.minY + imageHeight / 2 + CGFloat(i) * imageHeight)
        }
    }
    
    private func cropeGivenImage() {
        let puzzleImage = UIImage(data: puzzle.image!)!
        let widthInPixels = puzzleImage.size.width * puzzleImage.scale
        let heightInPixels = puzzleImage.size.height * puzzleImage.scale
        let width = widthInPixels * CGFloat(7) / CGFloat(countOfPuzzlesInEveryLine * 5 + 2)
        let height = heightInPixels * CGFloat(7) / CGFloat(countOfPuzzlesInEveryLine * 5 + 2)
        for i in 0..<componentsCount {
            let cropedImage = puzzleImage.cropImage(
                x: CGFloat(i % countOfPuzzlesInEveryLine) * widthInPixels * CGFloat(5) / CGFloat(countOfPuzzlesInEveryLine * 5 + 2),
                y:  CGFloat(i / countOfPuzzlesInEveryLine) * heightInPixels * CGFloat(5) / CGFloat(countOfPuzzlesInEveryLine * 5 + 2),
                width: width,
                height: height)
            componentsImagesSortedArray.append(cropedImage)
            componentsImageSet.insert(cropedImage)
            componentsImageArray = Array(componentsImageSet)
        }
        puzzleImageView.image! = puzzleImage.cropImage(x: widthInPixels / CGFloat(countOfPuzzlesInEveryLine * 5 + 2),
                                                       y: heightInPixels / CGFloat(countOfPuzzlesInEveryLine * 5 + 2),
                                                       width: widthInPixels * CGFloat(countOfPuzzlesInEveryLine * 5) / CGFloat(countOfPuzzlesInEveryLine * 5 + 2),
                                                       height: heightInPixels * CGFloat(countOfPuzzlesInEveryLine * 5) / CGFloat(countOfPuzzlesInEveryLine * 5 + 2))
        UIApplication.dataManager.saveAllComponents(for: puzzle.date!, componentsImages: componentsImagesSortedArray)
        componentsCollectionView.reloadData()
    }
    
    private func timeString(time: TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
}




extension CropedImageViewController: PuzzleAlertViewControllerDelegate, ShowCupAlertControllerDelegate {
    
    func didSelectItem(in controller: PuzzleAlertViewController, at index: Int) {
        switch index {
        case 1:
            startAnother()
        case 2:
            restart()
        default:
            break
        }
    }
    
    private func startAnother() {
        self.dismiss(animated: true, completion: nil)
        UIApplication.dataManager.resetPuzzleSettings(for: puzzle)
        navigationController?.popToViewController((navigationController?.viewControllers[1])!, animated: true)
    }
    
    private func restart() {
        self.dismiss(animated: true, completion: nil)
        self.timerLabel.backgroundColor = UIColor(red: CGFloat(2) / CGFloat(255), green: CGFloat(15) / CGFloat(255), blue: CGFloat(51) / CGFloat(255), alpha: 1)
        restartsGame = true
        for view in dropView.subviews {
            view.removeFromSuperview()
        }
        self.startNewGame()
    }
    
    func newGame() {
        self.dismiss(animated: false, completion: nil)
        navigationController?.popToViewController((navigationController?.viewControllers[1])!, animated: true)
    }
    
    func showPuzzle() {
        self.dismiss(animated: true)
        self.dropView.isUserInteractionEnabled = false
    }
}

extension CropedImageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return componentsImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ComponentImageCell
        addLongPressGesture(view: cell.contentView.subviews[0])
        cell.componentImageView.image = componentsImageArray[indexPath.row]
        cell.componentImageView.tag = Int(componentsImageArray[indexPath.row].accessibilityIdentifier!)!
        cell.componentImageView.cropAsPuzzle(count: countOfPuzzlesInEveryLine)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = componentsImageArray.remove(at: sourceIndexPath.item)
        componentsImageArray.insert(item, at: destinationIndexPath.item)
    }
}

