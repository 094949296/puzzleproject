//
//  ShowCupAlertController.swift
//  PuzzleProject
//
//  Created by Hayk Movsesyan on 1/15/19.
//  Copyright © 2019 Yurik Mnatsakanyan. All rights reserved.
//

import UIKit

protocol ShowCupAlertControllerDelegate: class {
    func newGame()
    func showPuzzle()
}

class ShowCupAlertController: UIViewController {
    
    var newStars: Int!
    var oldStars: Int!
    weak var delegate: ShowCupAlertControllerDelegate!
    @IBOutlet var starsImageView: [UIImageView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for viewIndex in 0 ..< newStars {
            starsImageView[viewIndex].image = UIImage(named: "star2")
        }
        if oldStars > newStars {
            for viewIndex in newStars ..< oldStars {
                starsImageView[viewIndex].image = UIImage(named: "star3")
            }
        }
    }
    
    @IBAction func startNewGame() {
        delegate.newGame()
    }
    
    @IBAction func notNow() {
        delegate.showPuzzle()
    }
    
}
