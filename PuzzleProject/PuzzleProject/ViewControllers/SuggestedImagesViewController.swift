//
//  SuggestedImagesViewController.swift
//  Created by Hayk Movsesyan on 12/24/18.
//  Copyright © 2018 Hayk Movsesyan. All rights reserved.
//

import UIKit
import AVFoundation

class SuggestedImagesViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var suggestedPuzzlesCollectionView: UICollectionView!
    let image = UIImagePickerController()
    var tappedAddPhoto: Bool!
    var suggestedPuzzles = [Puzzle]()
    var checkedPuzzles = [Puzzle]()
    var checkeds = 0
    var selected = false
    var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        suggestedPuzzles = UIApplication.dataManager.gettingPuzzles()
        suggestedPuzzlesCollectionView.reloadData()
    }
    
    @IBAction func selectTapped() {
        if !suggestedPuzzles.isEmpty {
            if selectButton.isSelected == false {
                selectButton.isSelected = true
                selected = true
                for index in 0 ..< suggestedPuzzlesCollectionView.visibleCells.count {
                    if let cell = suggestedPuzzlesCollectionView.visibleCells[index] as? AvailableImageCell {
                        cell.chackButton.isHidden = false
                    }
                }
            } else {
                if checkeds != 0 {
                    var checkedPuzzles = [Puzzle]()
                    for puzzle in suggestedPuzzles {
                        if puzzle.checked {
                            checkedPuzzles.append(puzzle)
                        }
                    }
                    suggestedPuzzles.removeAll { (puzzle: Puzzle) -> Bool in
                        puzzle.checked == true
                    }
                    UIApplication.dataManager.deletePuzzle(puzzles: checkedPuzzles)
                }
                selectButton.isSelected = false
                selected = false
                for index in 1 ..< suggestedPuzzlesCollectionView.visibleCells.count {
                    if let cell = suggestedPuzzlesCollectionView.visibleCells[index] as? AvailableImageCell {
                        cell.chackButton.isSelected = false
                        cell.chackButton.isHidden = true
                    }
                }
                suggestedPuzzlesCollectionView.reloadData()
            }
        }
    }
    
    @IBAction func backToPreviousPage() {
        navigationController?.popViewController(animated: true)
        for puzzle in suggestedPuzzles {
            puzzle.checked = false
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let vc = storyboard!.instantiateViewController(withIdentifier: "SelectedImageViewController") as! SelectedImageViewController
        if let image = info[.editedImage] as? UIImage {
            if image.size.width > image.size.height {
                vc.selectedImage = image.centerCrop()
                UIApplication.dataManager.savePuzzle(image: image.centerCrop()) { (addedPuzzle) in
                    vc.puzzle = addedPuzzle
                }
            } else {
                vc.selectedImage = image
                UIApplication.dataManager.savePuzzle(image: image) { (addedPuzzle) in
                    vc.puzzle = addedPuzzle
                }
            }
        }
        navigationController?.pushViewController(vc, animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    private func openSelectedImageToCrop(indexPath: IndexPath) {
        if indexPath.row != 0 {
            if suggestedPuzzles[indexPath.row - 1].completed == false {
                tappedAddPhoto = false
                let alert = PuzzleAlertViewController.init(with: "Do You Want", items: ["New Game", "Continue", "Cancel"])
                alert.delegate = self
                selectedIndexPath = indexPath
                self.present(alert, animated: true, completion: nil)
            } else {
                let vc = storyboard?.instantiateViewController(withIdentifier: "SelectedImageViewController") as! SelectedImageViewController
                vc.puzzle = suggestedPuzzles[indexPath.row - 1]
                vc.selectedImage = UIImage(data: suggestedPuzzles[indexPath.row - 1].image!)
                navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            tappedAddPhoto = true
            image.delegate = self
            image.allowsEditing = true
            let alert = PuzzleAlertViewController.init(with: nil, items: ["Add Images", "Take a Photo", "Cancel"])
            alert.delegate = self
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func selectImage(indexPath: IndexPath) {
        if indexPath.row != 0 {
            let cell = suggestedPuzzlesCollectionView.cellForItem(at: indexPath) as! AvailableImageCell
            if !cell.chackButton.isSelected {
                cell.chackButton.isSelected = true
                suggestedPuzzles[indexPath.row - 1].checked = true
                checkeds += 1
            } else {
                cell.chackButton.isSelected = false
                suggestedPuzzles[indexPath.row - 1].checked = false
                checkeds -= 1
            }
        }
    }
    
}

extension SuggestedImagesViewController: PuzzleAlertViewControllerDelegate {
    
    func didSelectItem(in controller: PuzzleAlertViewController, at index: Int) {
        switch index {
        case 0:
            if tappedAddPhoto == true {
                addImageFromGallery()
            }
        case 1:
            if tappedAddPhoto == true {
                takeAPhoto()
            } else {
                startsNewGame()
            }
        case 2:
            if tappedAddPhoto == true {
                cancel()
            } else {
                continueGame()
            }
        case 3:
            cancel()
        default:
            break
        }
    }
    
    private func startsNewGame() {
        if let indexPath = selectedIndexPath {
            self.dismiss(animated: true, completion: nil)
            let vc = storyboard?.instantiateViewController(withIdentifier: "SelectedImageViewController") as! SelectedImageViewController
            vc.puzzle = suggestedPuzzles[indexPath.row - 1]
            UIApplication.dataManager.resetPuzzleSettings(for: suggestedPuzzles[indexPath.row - 1])
            vc.selectedImage = UIImage(data: suggestedPuzzles[indexPath.row - 1].image!)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func continueGame() {
        self.dismiss(animated: true, completion: nil)
        if let indexPath = selectedIndexPath {
            let vc = storyboard?.instantiateViewController(withIdentifier: "CropedImageViewController") as! CropedImageViewController
            vc.puzzle = suggestedPuzzles[indexPath.row - 1]
            vc.startedNewGame = false
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func addImageFromGallery() {
        self.dismiss(animated: true, completion: nil)
        self.image.sourceType = .photoLibrary
        self.present(self.image, animated: true)
    }
    
    private func takeAPhoto() {
        self.dismiss(animated: true, completion: nil)
        self.image.sourceType = .camera
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.present(image, animated: true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    self.present(self.image, animated: true)
                }
            }
        case .denied:
            self.openSettings()
        case .restricted:
            self.openSettings()
        }
    }
    
    private func openSettings() {
        let alertController = UIAlertController (title: "You need to give Camera access", message: "Go to Settings?", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                if UIApplication.shared.canOpenURL(settingsURL) {
                    UIApplication.shared.open(settingsURL, completionHandler: nil)
                }
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private func cancel() {
        self.dismiss(animated: false, completion: nil)
    }
    
}

extension SuggestedImagesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return suggestedPuzzles.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row != 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! AvailableImageCell
            cell.puzzleImage.image = UIImage(data: suggestedPuzzles[indexPath.row - 1].image!)
            for starIndex in 0 ..< 5 {
                if starIndex < Int(suggestedPuzzles[indexPath.row - 1].stars) {
                    cell.starsImageViewCollection![starIndex].image = UIImage(named: "star2")
                } else {
                    cell.starsImageViewCollection![starIndex].image = UIImage(named: "star1")
                }
            }
            cell.chackButton.isHidden = true
            if selected == false {
                cell.chackButton.isHidden = true
            } else {
                cell.chackButton.isHidden = false
                if !suggestedPuzzles[indexPath.row - 1].checked {
                    cell.chackButton.isSelected = false
                } else {
                    cell.chackButton.isSelected = true
                }
            }
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addNewImageCell", for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width / 2 - 15 , height: view.frame.size.width / 2 - 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !selectButton.isSelected {
            openSelectedImageToCrop(indexPath: indexPath)
        } else {
            selectImage(indexPath: indexPath)
        }
    }
}
