//
//  ButtonAnimation.swift
//  a
//
//  Created by Hayk Movsesyan on 12/24/18.
//  Copyright © 2018 Hayk Movsesyan. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func animate() {
        let animation = CASpringAnimation(keyPath: "transform.scale")
        animation.duration = 2
        animation.fromValue = 1
        animation.toValue = 1.05
        animation.autoreverses = true
        animation.repeatCount = .infinity
        animation.initialVelocity = 0.5
        animation.damping = 4.0
        layer.add(animation, forKey: nil)
    }
    
}
