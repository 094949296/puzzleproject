//
//  ImageCroping.swift
//  PuzzleProject
//
//  Created by Yurik Mnatsakanyan on 1/11/19.
//  Copyright © 2019 Yurik Mnatsakanyan. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func cropImage(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) -> UIImage {
        let crop = CGRect(x: x, y: y, width: width, height: height)
        let cgImage = self.cgImage?.cropping(to: crop)
        let cropedImage: UIImage = UIImage(cgImage: cgImage!)
        return cropedImage
    }
    
    func centerCrop() -> UIImage {
        let width = size.width
        let height = size.height
        if width > height {
            return cropImage(x: (width - height)/2, y: 0, width: height, height: height)
        } else {
            return cropImage(x: 0, y: (width - height)/2, width: width, height: width)
        }
    }
    
}
