//
//  SetPuzzleStyleOnImageView.swift
//  PuzzleProject
//
//  Created by Hayk Movsesyan on 1/17/19.
//  Copyright © 2019 Yurik Mnatsakanyan. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func cropAsPuzzle(count puzzlesInLine : Int) {
        var pointsArray: [CGPoint] = []
        let width = self.frame.width / CGFloat(1.4)
        let height = self.frame.height / CGFloat(1.4)
        let i = self.tag
        
        pointsArray.append(CGPoint(x: width * CGFloat(0.2), y: height * CGFloat(0.2)))
        if i >= puzzlesInLine {
            pointsArray.append(CGPoint(x: width * CGFloat(0.6), y: height * CGFloat(0.2)))
            if (((i / puzzlesInLine) % 2 == 0 && i % 2 == 0) || ((i / puzzlesInLine) % 2 == 1 && i % 2 == 1) && puzzlesInLine % 2 == 0) || (i % 2 == 0 && puzzlesInLine % 2 == 1) {
                pointsArray.append(CGPoint(x: width * CGFloat(0.5), y: height * CGFloat(0.02)))
                pointsArray.append(CGPoint(x: width * CGFloat(0.7), y: height * CGFloat(0)))
                pointsArray.append(CGPoint(x: width * CGFloat(0.9), y: height * CGFloat(0.02)))
            } else {
                pointsArray.append(CGPoint(x: width * CGFloat(0.5), y: height * CGFloat(0.38)))
                pointsArray.append(CGPoint(x: width * CGFloat(0.7), y: height * CGFloat(0.4)))
                pointsArray.append(CGPoint(x: width * CGFloat(0.9), y: height * CGFloat(0.38)))
            }
            pointsArray.append(CGPoint(x: width * CGFloat(0.8), y: height * CGFloat(0.2)))
        }
        pointsArray.append(CGPoint(x: width * CGFloat(1.2), y: height * CGFloat(0.2)))
        if i % puzzlesInLine + 1 != puzzlesInLine {
            pointsArray.append(CGPoint(x: width * CGFloat(1.2), y: height * CGFloat(0.6)))
            if (((i / puzzlesInLine) % 2 == 0 && i % 2 == 0) || ((i / puzzlesInLine) % 2 == 1 && i % 2 == 1) && puzzlesInLine % 2 == 0) || (i % 2 == 0 && puzzlesInLine % 2 == 1) {
                pointsArray.append(CGPoint(x: width * CGFloat(1.02), y: height * CGFloat(0.5)))
                pointsArray.append(CGPoint(x: width * CGFloat(1), y: height * CGFloat(0.7)))
                pointsArray.append(CGPoint(x: width * CGFloat(1.02), y: height * CGFloat(0.9)))
            } else {
                pointsArray.append(CGPoint(x: width * CGFloat(1.38), y: height * CGFloat(0.5)))
                pointsArray.append(CGPoint(x: width * CGFloat(1.4), y: height * CGFloat(0.7)))
                pointsArray.append(CGPoint(x: width * CGFloat(1.38), y: height * CGFloat(0.9)))
            }
            pointsArray.append(CGPoint(x: width * CGFloat(1.2), y: height * CGFloat(0.8)))
        }
        pointsArray.append(CGPoint(x: width * CGFloat(1.2), y: height * CGFloat(1.2)))
        if i / puzzlesInLine + 1 != puzzlesInLine {
            pointsArray.append(CGPoint(x: width * CGFloat(0.8), y: height * CGFloat(1.2)))
            if (((i / puzzlesInLine) % 2 == 0 && i % 2 == 0) || ((i / puzzlesInLine) % 2 == 1 && i % 2 == 1) && puzzlesInLine % 2 == 0) || (i % 2 == 0 && puzzlesInLine % 2 == 1) {
                pointsArray.append(CGPoint(x: width * CGFloat(0.9), y: height * CGFloat(1.38)))
                pointsArray.append(CGPoint(x: width * CGFloat(0.7), y: height * CGFloat(1.4)))
                pointsArray.append(CGPoint(x: width * CGFloat(0.5), y: height * CGFloat(1.38)))
            } else {
                pointsArray.append(CGPoint(x: width * CGFloat(0.9), y: height * CGFloat(1.02)))
                pointsArray.append(CGPoint(x: width * CGFloat(0.7), y: height * CGFloat(1)))
                pointsArray.append(CGPoint(x: width * CGFloat(0.5), y: height * CGFloat(1.02)))
            }
            pointsArray.append(CGPoint(x: width * CGFloat(0.6), y: height * CGFloat(1.2)))
        }
        pointsArray.append(CGPoint(x: width * CGFloat(0.2), y: height * CGFloat(1.2)))
        if i % puzzlesInLine != 0 {
            pointsArray.append(CGPoint(x: width * CGFloat(0.2), y: height * CGFloat(0.8)))
            if (((i / puzzlesInLine) % 2 == 0 && i % 2 == 0) || ((i / puzzlesInLine) % 2 == 1 && i % 2 == 1) && puzzlesInLine % 2 == 0) || (i % 2 == 0 && puzzlesInLine % 2 == 1) {
                pointsArray.append(CGPoint(x: width * CGFloat(0.38), y: height * CGFloat(0.9)))
                pointsArray.append(CGPoint(x: width * CGFloat(0.4), y: height * CGFloat(0.7)))
                pointsArray.append(CGPoint(x: width * CGFloat(0.38), y: height * CGFloat(0.5)))
            } else {
                pointsArray.append(CGPoint(x: width * CGFloat(0.02), y: height * CGFloat(0.9)))
                pointsArray.append(CGPoint(x: width * CGFloat(0), y: height * CGFloat(0.7)))
                pointsArray.append(CGPoint(x: width * CGFloat(0.02), y: height * CGFloat(0.5)))
            }
            pointsArray.append(CGPoint(x: width * CGFloat(0.2), y: height * CGFloat(0.6)))
        }
        
        setPuzzleLayerToImages(i: i, imageView: self, countOfLine: puzzlesInLine, pointsArray: pointsArray)
    }
    
    func setPuzzleLayerToImages(i: Int, imageView: UIImageView, countOfLine: Int, pointsArray: [CGPoint]) {
        let puzzlePath = UIBezierPath()
        var _index = 0
        var index:Int {
            get {
                _index += 1
                return _index - 1
            }
        }
        puzzlePath.move(to: pointsArray[index])
        if i >= countOfLine {
            puzzlePath.addLine(to: pointsArray[index])
            puzzlePath.addQuadCurve(to: pointsArray[index + 1], controlPoint: pointsArray[index - 1])
            puzzlePath.addQuadCurve(to: pointsArray[index + 1], controlPoint: pointsArray[index - 1])
        }
        puzzlePath.addLine(to: pointsArray[index])
        if i % countOfLine + 1 != countOfLine {
            puzzlePath.addLine(to: pointsArray[index])
            puzzlePath.addQuadCurve(to: pointsArray[index + 1], controlPoint: pointsArray[index - 1])
            puzzlePath.addQuadCurve(to: pointsArray[index + 1], controlPoint: pointsArray[index - 1])
        }
        puzzlePath.addLine(to: pointsArray[index])
        if i/countOfLine + 1 != countOfLine {
            puzzlePath.addLine(to: pointsArray[index])
            puzzlePath.addQuadCurve(to: pointsArray[index + 1], controlPoint: pointsArray[index - 1])
            puzzlePath.addQuadCurve(to: pointsArray[index + 1], controlPoint: pointsArray[index - 1])
        }
        puzzlePath.addLine(to: pointsArray[index])
        if i % countOfLine != 0 {
            puzzlePath.addLine(to: pointsArray[index])
            puzzlePath.addQuadCurve(to: pointsArray[index + 1], controlPoint: pointsArray[index - 1])
            puzzlePath.addQuadCurve(to: pointsArray[index + 1], controlPoint: pointsArray[index - 1])
        }
        puzzlePath.close()
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = puzzlePath.cgPath
        imageView.layer.mask = shapeLayer
        
    }
}
